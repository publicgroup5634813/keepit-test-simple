package Main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.swing.*;

public class SeleniumTest {

    public static void main(String[] args) {

        ms360Test();
    }

    public static void ms360Test(){

        System.getProperty("webdriver.chrome.driver", "D:/Java/driver/chromedriver_win32");

        WebDriver driver = new ChromeDriver();

        driver.get("https://ws-test.keepit.com");

        WebElement email = driver.findElement(By.name("email"));
        WebElement password = driver.findElement(By.name("password"));

        WebElement button = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/form/div[2]/div[2]/button"));

        email.sendKeys("automation_test@keepitqa.com");
        password.sendKeys("U*dJ1F*e4*");
        button.click();

        try {
            Thread.sleep(1000); // Adjust sleep time according to your application
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement connectorPage = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[3]/div[1]/div[2]/a/div[1]"));
        connectorPage.click();

        try {
            Thread.sleep(1000); // Adjust sleep time according to your application
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement addConnector = driver.findElement(By.xpath("//*[@id=\"root\"]/div/main/div[2]/div/div[1]/div/div/div/div[1]/div[1]/div/div/div"));
        addConnector.click();

        WebElement msConnector = driver.findElement(By.xpath("//*[@id=\"root\"]/div/main/div[2]/div/div[1]/div/div/div/div[1]/div[1]/div/div/div[2]/div[1]/span"));
        if (msConnector.isDisplayed()){
            System.out.println("PASS: MS365 connector found");
        } else {
            System.out.println("FAILED: MS365 connector not found");
        }

        driver.quit();

    }
}
