package Main;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Scanner;

public class ApiClientTest {

    public static void main(String[] args) {

        String endpoint = "https://ws-test.keepit.com/users/hbnbdm-sw0tha-0j8g61";
        String username = "automation_test@keepitqa.com";
        String password = "U*dJ1F*e4*";

        String responce = getUserInfo(endpoint, username, password);
        System.out.println("Responce: " + responce );
    }

    public static String getUserInfo(String endpoint, String username, String password ) {

        StringBuilder stringbuilder = new StringBuilder();

        try{
            URL url = new URL(endpoint);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            String loginString = username + ":" + password;
            String encLoginString = Base64.getEncoder().encodeToString(loginString.getBytes(StandardCharsets.UTF_8));
            connection.setRequestProperty("Authorization", "Basic " + encLoginString);

            int resCode = connection.getResponseCode();

            if (resCode == HttpURLConnection.HTTP_OK){

                System.out.println("Login successful");
                System.out.println("Response code: " + resCode);

                Scanner scanner = new Scanner(connection.getInputStream());

                while (scanner.hasNextLine()){
                    stringbuilder.append(scanner.nextLine());
                }
                scanner.close();
            } else {
                System.out.println("Failed with code: " + resCode);
            }

            connection.disconnect();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        return stringbuilder.toString();
    }
}